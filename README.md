# <%= fn_name %>

[![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?style=for-the-badge)](LICENSE)

> <%= fn_description %>


## Deployment

To be able to deploy this function on Google Cloud, you must create a project and a service account. [more](https://cloud.google.com/functions/docs/concepts/iam)






## Support

<a href="https://www.buymeacoffee.com/Z1Bu6asGV" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: auto !important;width: auto !important;" ></a>
