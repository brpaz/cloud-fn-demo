PKG := "fn"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.DEFAULT_GOAL := help
.PHONY: build clean test lint help

lint: ## Lint the files
	@revive -config revive.toml

fmt: ## Formats the Go source code
	@gofmt -s -w $(GO_FILES)

test: ## Run unittests
	@CGO_ENABLED=0 go test -v ./...

build: clean ## Build the binary file
	@go build -i -v -o build/app .

clean: ## Remove previous build
	@rm -f build

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
